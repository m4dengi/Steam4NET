// This file is automatically generated.
using System;
using System.Text;
using System.Runtime.InteropServices;
using Steam4NET.Attributes;

namespace Steam4NET
{

	public interface IClientNetworkingSocketsSerialized
	{
		[VTableSlot(0)]
		Int32 SendP2PRendezvous(CSteamID arg0, UInt32 arg1, Byte[] arg2, UInt32 arg3);
		[VTableSlot(1)]
		Int32 SendP2PConnectionFailure(CSteamID arg0, UInt32 arg1, UInt32 arg2, string arg3);
		[VTableSlot(2)]
		Int32 GetCertAsync();
		[VTableSlot(3)]
		Int32 GetNetworkConfigJSON(Byte[] arg0, UInt32 arg1);
		[VTableSlot(4)]
		Int32 CacheRelayTicket(Byte[] arg0, UInt32 arg1);
		[VTableSlot(5)]
		Int32 GetCachedRelayTicketCount();
		[VTableSlot(6)]
		Int32 GetCachedRelayTicket(UInt32 arg0, Byte[] arg1, UInt32 arg2);
		[VTableSlot(7)]
		Int32 PostConnectionStateMsg(Byte[] arg0, UInt32 arg1);
		[VTableSlot(8)]
		Int32 TEST_ClearInMemoryCachedCredentials();
		[VTableSlot(9)]
		Int32 TEST_GetNetworkConfigLocalFilename();
		[VTableSlot(10)]
		Int32 TEST_ClearCachedNetworkConfig();
	};
}
